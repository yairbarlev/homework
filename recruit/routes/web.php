<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*Hello world route*/

Route::get('/hello', function () {
    return 'Hello World! This is my first ever Laravel app';
});
/*
This function is called ‘closure function’

Url: http://localhost/recruit/public/hello

Getting parameters required + name  
*/
Route::get('/student/{id}', function ($id = 'No student found') {
    return 'We got student no. '.$id;
})->name('students');
/*
Url: http://localhost/recruit/public/student/5
*/
Route::get('/car/{?id}', function ($id = 'No car found') {
    return 'We got car no. '.$id;
})->name('cars');
/*
Url: http://localhost/recruit/public/car
*/
Route::get('/users/{?Name}', function ($Name = 'name missing' , $Email = 'email missing') {
    if(isset($Email)){
        //TODO: validate for integer  
        return 'your name is: '.$Name.' and your email is: '.$Email; 
    } else {
        return 'We need the email for the user';
    }
        

})->name('users');
