<?php
  class Htmlpage{
        protected $title = "example Title";
        protected $body = "examle Body";
        function __construct($text = "" ,$body = ""){
                if($text != ""){
                    $this->title = $text;
                }
                if($body != ""){
                    $this->body = $body;
                }
        }
        public function view(){
            echo "<html>
                 <head>
                       <title>$this->title</title>
                 </head>
                <body>$this->body
                </body>
                 </html>";
        }
    }
class Htmlpagecolor extends Htmlpage{
    protected $color = 'red';
    public function __set($property, $value){
        if($property == 'color'){
            $colors = array('red','yellow','green','black','white','brown','orange','blue','pink','grey','purple');
            if(in_array($value,$colors)){
                $this->color=$value ;     
            } else{
                     echo "Error:  you picked a wrong color, The colores available are: 'red','yellow','green','black','white','brown','orange','blue','pink','grey','purple'";
            }
        }
        
    }
    public function view(){
        echo "<html>
            <head>
                    <title>  $this->title  </title>
            </head>
            <body> <p style = 'color:$this->color'> $this->body</p>
            </body>
            </html>";
        }
    }
    class Htmlpagefull extends Htmlpagecolor{
        protected $fontSize = 10;
        protected $color = 'red';
        public function __set($property, $value){
            $goodVal = array(10,11,12,13,14,15,16,17,18,19,20,21,22,23,24);
            if($property == 'color'){
                $colors = array('red','yellow','green','black','white','brown','orange','blue','pink','grey','purple');
                if(in_array($value,$colors)){
                    $this->color=$value ;
                } else{
                         echo "Error:  you picked a wrong color, The colores available are: 'red','yellow','green','black','white','brown','orange','blue','pink','grey','purple'";
                }
            }
            elseif($property == 'fontSize'){

                if(in_array($value,$goodVal)){
                    $this->fontSize=$value."px" ;     
                }
                else{
                         echo "Error:  you picked a wrong font the fonr range is 10 - 24";
                }
            }
            else{
                echo "Error:  you picked a property";
            }
        }
        public function view(){
            echo "<html>
                <head>
                        <title>  $this->title  </title>
                </head> <h1 style = 'color:$this->color'>
                <body> <p style = 'color:$this->color' style='font-size:$this->fontSize'>$this->body</p>
                </body></h1>
                </html>";
            }
        }
?>