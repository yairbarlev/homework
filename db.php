<?php

class DB {
    # defining assential veriables
    # database name
    protected $db_name;
    # database user
    protected $db_user;
    # database password
    protected $db_pass;
    # database host
    protected $db_host;
    # database
    protected $db;
    # constructor
    function __construct($db_host,$db_name,$db_user,$db_pass){
        #initializing
        $this->db_name = $db_name;
        $this->db_user = $db_user;
        $this->db_pass = $db_pass;
        $this->db_host = $db_host;

    }

    public function connect(){
        #connecting to the database
        $this->db = new mysqli($this ->db_host,$this->db_user,$this ->db_pass,$this->db_name);
        if(mysqli_connect_errno()){
            printf("Connection failed %s",mysqli_connect_error());
        }
        return $this->db;
    }






}








?>